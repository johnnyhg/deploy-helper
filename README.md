# deploy-helper

## 介绍
`deploy-helper`是一個Spring Boot & Spring Cloud项目自动远程部署轻量级Maven插件.

通常情况下,我们将开发好的Spring Boot程序部署到服务器上时，会经历以下流程：

1、maven项目打包

2、通过sftp、scp软件将jar包拷贝到目标服务器上

3、通过`ps`命令查找pid进程号，使用`kill`命令杀进程

4、通过java命令启动jar包

如果需求更改后,我们需要重复以上流程，对于开发者来说，是一件很无聊的事情,特别是需要发布到测试服务器等进行测试的情况下，非常耗时间。

`deploy-helper`解决的就是步骤2、步骤3、步骤4,项目打包好后,执行插件完成自动化部署，整个过程耗时10s左右

## 软件架构
软件架构说明


## 安装教程

在maven項目的`pom.xml`中直接引入

```xml
<plugin>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>deploy-helper-maven-plugin</artifactId>
    <!--请在maven中央仓库搜索最新版本-->
    <version>1.2</version>
    <configuration>
        <!--配置文件路径,由于密码是明文模式,该配置文件可以不配置在项目中,防止服务器密码泄漏,同时建议部署程序使用非root用户-->
        <configurationFile>/mnt/home/deployHelperConfig.xml</configurationFile>
    </configuration>
</plugin>
```

`deployHelperConfig.xml`配置文件在项目的`src/main/resources`目录下时,可以不用在`pom.xml`里面进行配置

引入后,在项目中可以看到我们引入的插件

![](deployPlugin.png)

## 使用说明

###　配置文件

`deploy-helper`在执行时,需要传递一个xml文件作为初始化文件,xml代表了所需要进行自动化部署的配置信息，`XML`模板如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<deployHelperConfiguration>
    <!--指定deployHelper自动上传时所连接的服务器以及要使用的deploy配置-->
    <profile server="dev" deploy="dev"></profile>
    <servers>
        <!--可以配置多个server节点,代表的是目标服务器-->
        <server profile="dev" host="192.168.0.223" port="22" username="root" password="123456"></server>
    </servers>
    <deploys>
        <!--目标服务器存在启动&停止脚本-->
        <deploy profile="dev"
                source="target/demo-http-1.0.jar"
                target="/home/xiaoymin/test/test1/demo-http-1.0.jar"
                processName="demo-http-1.0.jar"
                activeDefaultStart="true"
                backup="true"
                startShell="/home/xiaoymin/test/test1/startup.sh"
                stopShell="/home/xiaoymin/test/test1/stop.sh">
        </deploy>
        <!--不存在启动脚本,使用deploy-helper默认的脚本-->
        <deploy profile="test"
                source="target/demo-http-1.0.jar"
                target="/home/xiaoymin/test/test5/demo-http-1.0.jar"
                processName="demo-http-1.0.jar"
                activeDefaultStart="true">
        </deploy>
    </deploys>
</deployHelperConfiguration>
```

在上面的XML模板中,主要包含三个节点

- profile：`profile`节点代表的是在插件执行时具体引用那个`server`和`deploy`
- servers:`Linux`服务器节点配置，主要包含的属性：profile(名称)、host(主机号)、port(端口号)、username(登陆用户名)、password(登陆密码)
- deploy：该节点是最终做自动化部署时的配置,可以配置多个,在profile节点时可以自动指定,区分开发环境&生成环境，属性如下：
  - profile:名称
  - source:需要上传的jar文件,可以是相对目录下的文件,也可以使用绝对路径
  - target:目标服务器路径,需要注意的时该属性必须配置完整的`路径`+`文件名`,不能只包含目录,上传是覆盖策略
  - processName:该参数主要用于在服务上用于查询进程号的名称，越完整越好,通过该名称通过`ps`命令查找时可以精准定位到程序的`pid`,如果`stopShell`停止脚本存在的话则该参数可以不传
  - backup: 由于插件目前的策略是直接覆盖服务器上文件,有时候我们希望保留上一个版本,此时可以设置该参数为true,帮助我们进行备份,该参数为true时,会在目标目录创建一个backup的文件夹,将服务器的文件进行备份(自1.1版本开始)
  - activeDefaultStart：是否自动生成启动jar包的启动命令，该参数的优先级低于`startShell`
  - startShell:启动脚本，必须配置完整路径
  - stopShell:停止脚本,必须配置完整路径

### 脚本参考

目标服务器上配置的启动脚本和停止脚本参考如下：

**启动脚本(startup.sh)**:

```shell
nohup java -jar demo-http-1.0.jar &
```

**停止脚本(stop.sh)**:

```shell
#!/bin/bash
echo "Stop Procedure : demo-http-1.0.jar"
pid=`ps -ef |grep java|grep demo-http-1.0.jar|awk '{print $2}'`
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill $pid
fi
```

## 使用场景

目前使用`deploy-helper`插件主要区分2种场景

### 目标服务器存在启动&停止脚本

我们可以通过在部署服务器上线创建2个脚本,分别是`启动脚本`和`停止脚本`,此时，我们需要在`deployHelperConfig.xml`配置文件中配置`deploy`节点，示例如下：

```xml
<deploy profile="dev"
        source="target/demo-http-1.0.jar"
        target="/home/xiaoymin/test/test1/demo-http-1.0.jar"
        startShell="/home/xiaoymin/test/test1/startup.sh"
        stopShell="/home/xiaoymin/test/test1/stop.sh">
</deploy>
```

通过上面的配置文件，我们可以知道

部署目录：`/home/xiaoymin/test/test1`,如果该目录不存在,则会自动创建该目录

启动脚本：`/home/xiaoymin/test/test1/startup.sh`

停止脚本：`/home/xiaoymin/test/test1/stop.sh`

需要注意的是，如果使用这种场景,那么目标服务器的启动脚本和停止脚本文件必须存在,并且**必须是可执行的**,需要有Linux的`x`权限

### 使用`deploy-helper`提供的默认脚本

如果你嫌弃写`shell`脚本麻烦,那么`deploy-helper`默认提供了启动和停止的脚本，此时，配置`deploy`的节点时如下：

```xml
<deploy profile="test"
        source="target/demo-http-1.0.jar"
        target="/home/xiaoymin/test/test5/demo-http-1.0.jar"
        processName="demo-http-1.0.jar"
        backup="true"
        activeDefaultStart="true">
</deploy>
```

通过上面的配置：

部署目录：`/home/xiaoymin/test/test5/`,如果该目录不存在,则会自动创建该目录

进程名称：`processName`在这种模式下必须设置该参数，并且最好是通过`ps`命令能查找到`pid`进程号的,所以需要提供完整的名称最好

启动自动启动：`activeDefaultStart`命令优先级低于`startShell`,如果`startShell`启动命令不为空,程序会使用`startShell`中的命令进行启动程序,否则`activeDefaultStart`参数必须设置为`true`，此时,使用`deploy-helper`默认会生成jar项目的启动命令进行启动

备份策略：`backup`参数为true时,会对目标文件进行备份