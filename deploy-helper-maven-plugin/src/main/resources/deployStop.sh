#!/bin/bash
echo "Stop Procedure : $processName"
pid=`ps -ef |grep java|grep $processName|awk '{print $2}'`
echo 'old Procedure pid:'$pid
if [ -n "$pid" ]
then
kill $pid
fi
