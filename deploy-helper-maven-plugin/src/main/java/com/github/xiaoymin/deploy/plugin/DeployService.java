/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */
package com.github.xiaoymin.deploy.plugin;

import com.github.xiaoymin.deploy.model.DeployRequest;

/***
 * 执行目标行为公共插件
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/05 13:00
 */
public interface DeployService {

    /**
     * 执行插件动作
     * @param deployRequest 请求参数
     * @return 是否操作成功
     */
    boolean action(DeployRequest deployRequest);

    /**
     * 获取Plugin名称
     * @return 插件名称
     */
    String getPluginName();
}
