/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.model;

import com.github.xiaoymin.deploy.DeployHelperConnection;
import org.apache.maven.project.MavenProject;

/***
 * 执行DeployService的请求参数封装
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/05 13:33
 */
public class DeployRequest {

    private final DeployHelperConnection connection;
    private final DeployHelperDeploy helperDeploy;
    private final MavenProject mavenProject;

    public DeployRequest(DeployHelperConnection connection, DeployHelperDeploy helperDeploy, MavenProject mavenProject) {
        this.connection = connection;
        this.helperDeploy = helperDeploy;
        this.mavenProject = mavenProject;
    }

    public DeployHelperConnection getConnection() {
        return connection;
    }

    public DeployHelperDeploy getHelperDeploy() {
        return helperDeploy;
    }

    public MavenProject getMavenProject() {
        return mavenProject;
    }
}
