/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.common;

/***
 * 一些常量
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/05 13:20
 */
public class PluginConst {

    /**
     * 根路径
     */
    public static final String ROOT_PREFIX="/";

    /**
     * 停止服务
     */
    public static final String PLUGIN_STOP_SERVICE="PLUGIN_STOP_SERVICE";
    /**
     * 启动服务
     */
    public static final String PLUGIN_START_SERVICE="PLUGIN_START_SERVICE";

    /**
     * 备份目标文件
     */
    public static final String PLUGIN_BACKUP_SERVICE="PLUGIN_BACKUP_SERVICE";

    /**
     * 上传
     */
    public static final String PLUGIN_UPLOAD_SERVICE="PLUGIN_UPLOAD_SERVICE";
}
