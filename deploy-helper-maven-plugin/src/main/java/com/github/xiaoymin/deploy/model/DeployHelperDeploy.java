/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.model;

import cn.hutool.core.util.StrUtil;

/***
 * 目标文件进行部署时所依赖的配置文件类
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/02 11:01
 */
public class DeployHelperDeploy {
    private String profile;
    private String processName;
    private String target;
    private String source;
    /**
     * 是否备份原目标文件,默认为false
     */
    private boolean backup=false;
    /**
     * 使用默认启动命令
     * nohup java -target-xxx.jar &
     */
    private boolean activeDefaultStart=false;
    private String startShell;
    private String stopShell;

    public boolean isBackup() {
        return backup;
    }

    public void setBackup(boolean backup) {
        this.backup = backup;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public boolean isActiveDefaultStart() {
        return activeDefaultStart;
    }

    public void setActiveDefaultStart(boolean activeDefaultStart) {
        this.activeDefaultStart = activeDefaultStart;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStartShell() {
        return startShell;
    }

    public void setStartShell(String startShell) {
        this.startShell = startShell;
    }

    public String getStopShell() {
        return stopShell;
    }

    public void setStopShell(String stopShell) {
        this.stopShell = stopShell;
    }

    @Override
    public String toString() {
        return "DeployHelperDeploy{" +
                "profile='" + profile + '\'' +
                ", processName='" + processName + '\'' +
                ", target='" + target + '\'' +
                ", source='" + source + '\'' +
                ", activeDefaultStart=" + activeDefaultStart +
                ", startShell='" + startShell + '\'' +
                ", stopShell='" + stopShell + '\'' +
                '}';
    }

    /**
     * 校验参数
     */
    public void validate(){
        if (StrUtil.isBlank(this.target)){
            throw new IllegalArgumentException("target can't be empty!!!");
        }
        if (StrUtil.isBlank(this.source)){
            throw new IllegalArgumentException("source can't be empty!!!");
        }
        if (StrUtil.isBlank(this.startShell)){
            if (!this.activeDefaultStart){
                throw new IllegalArgumentException("startShell can't be empty!!!");
            }
        }
        if (StrUtil.isBlank(this.stopShell)){
            //判断processName是否为空,二者必须有一个不能为空
            if (StrUtil.isBlank(this.processName)){
                throw new IllegalArgumentException("Either stopShell or processName must be one and cannot be empty!!!");
            }
        }

    }
}
