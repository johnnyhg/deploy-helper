/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.model;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/***
 * 主要配置文件
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/02 11:00
 */
public class DeployHelperConfiguration {

    public static final String ROOT_CONFIG_NAME="deployHelperConfiguration";

    Logger logger= LoggerFactory.getLogger(DeployHelperConfiguration.class);

    private String serverProfile;
    private String deployProfile;
    private DeployHelperServer profileServer;
    private DeployHelperDeploy profileDeploy;
    private List<DeployHelperServer> servers=new ArrayList<DeployHelperServer>();
    private List<DeployHelperDeploy> deploys=new ArrayList<DeployHelperDeploy>();

    /**
     * 初始化读取Xml配置文件
     * @param file 初始化文件
     * @throws DocumentException 解析Xml异常
     */
    public void load(File file) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document=reader.read(file);
        Element rootElement=document.getRootElement();
        if (StrUtil.equals(rootElement.getName(), ROOT_CONFIG_NAME)){
            Element profileElement=rootElement.element("profile");
            setServerProfile(profileElement.attributeValue("server"));
            setDeployProfile(profileElement.attributeValue("deploy"));
            logger.info("serverProfile:{},deployProfile:{}",getServerProfile(),getDeployProfile());
            //获取server节点
            Element serversElement=rootElement.element("servers");
            List<Element> serverElementLists=serversElement.elements("server");
            if(CollectionUtil.isNotEmpty(serverElementLists)){
                serverElementLists.forEach(serverElment->{
                    DeployHelperServer deployHelperServer=new DeployHelperServer();
                    deployHelperServer.setProfile(serverElment.attributeValue("profile"));
                    deployHelperServer.setUsername(serverElment.attributeValue("username"));
                    deployHelperServer.setPassword(serverElment.attributeValue("password"));
                    deployHelperServer.setPort(Integer.parseInt(Objects.toString(StrUtil.trimToNull(serverElment.attributeValue("port")),"22")));
                    deployHelperServer.setHost(serverElment.attributeValue("host"));
                    this.servers.add(deployHelperServer);
                });
            }
            //获取server节点
            Element deploysElement=rootElement.element("deploys");
            List<Element> deployElementLists=deploysElement.elements("deploy");
            if (CollectionUtil.isNotEmpty(deployElementLists)){
                deployElementLists.forEach(deploy->{
                    DeployHelperDeploy deployHelperDeploy=new DeployHelperDeploy();
                    deployHelperDeploy.setProfile(deploy.attributeValue("profile"));
                    deployHelperDeploy.setSource(deploy.attributeValue("source"));
                    deployHelperDeploy.setTarget(deploy.attributeValue("target"));
                    deployHelperDeploy.setProcessName(deploy.attributeValue("processName"));
                    deployHelperDeploy.setStartShell(deploy.attributeValue("startShell"));
                    deployHelperDeploy.setStopShell(deploy.attributeValue("stopShell"));
                    deployHelperDeploy.setBackup(Boolean.valueOf(deploy.attributeValue("backup")));
                    deployHelperDeploy.setActiveDefaultStart(Boolean.valueOf(deploy.attributeValue("activeDefaultStart")));
                    this.deploys.add(deployHelperDeploy);
                });
            }
        }
    }

    /**
     * 根据配置文件切换profile
     */
    public void profile(){
        logger.info("Switch Profile");
        servers.forEach(deployHelperServer -> {
            if (StrUtil.equals(deployHelperServer.getProfile(),this.getServerProfile())){
                setProfileServer(deployHelperServer);
            }
        });
        deploys.forEach(deployHelperDeploy -> {
            if (StrUtil.equals(deployHelperDeploy.getProfile(),getDeployProfile())){
                setProfileDeploy(deployHelperDeploy);
            }
        });
    }

    public String getServerProfile() {
        return serverProfile;
    }

    public void setServerProfile(String serverProfile) {
        this.serverProfile = serverProfile;
    }

    public String getDeployProfile() {
        return deployProfile;
    }

    public void setDeployProfile(String deployProfile) {
        this.deployProfile = deployProfile;
    }

    public List<DeployHelperServer> getServers() {
        return servers;
    }

    public void setServers(List<DeployHelperServer> servers) {
        this.servers = servers;
    }

    public List<DeployHelperDeploy> getDeploys() {
        return deploys;
    }

    public void setDeploys(List<DeployHelperDeploy> deploys) {
        this.deploys = deploys;
    }

    public DeployHelperServer getProfileServer() {
        return profileServer;
    }

    public void setProfileServer(DeployHelperServer profileServer) {
        this.profileServer = profileServer;
    }

    public DeployHelperDeploy getProfileDeploy() {
        return profileDeploy;
    }

    public void setProfileDeploy(DeployHelperDeploy profileDeploy) {
        this.profileDeploy = profileDeploy;
    }
}
