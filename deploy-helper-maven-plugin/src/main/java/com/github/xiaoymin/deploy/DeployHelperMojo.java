/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/***
 * Mojo主class
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/02 10:32
 */
@Mojo(name = "deploy-helper", defaultPhase = LifecyclePhase.NONE,
        requiresDependencyResolution = ResolutionScope.NONE)
public class DeployHelperMojo extends AbstractMojo {

    Logger logger= LoggerFactory.getLogger(DeployHelperMojo.class);

    /**
     * Maven Project.
     *
     */
    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;

    /**
     * Location of the configuration file.
     */
    @Parameter(property = "deploy.helper.configurationFile",
            defaultValue = "${project.basedir}/src/main/resources/deployHelperConfig.xml", required = true)
    private File configurationFile;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        logger.info("DeployHelper start...");
        if (configurationFile!=null){
            DeployHelper.me.setMavenProject(this.project);
            DeployHelper.me.initConfig(configurationFile);
            DeployHelper.me.execute();
            logger.info("DeployHelper finished.");
        }else{
            logger.error("deployHelperConfig.xml can't found");
        }

    }
}
