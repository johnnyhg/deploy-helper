/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.plugin.impl;

import cn.hutool.core.util.StrUtil;
import com.github.xiaoymin.deploy.DeployHelperConnection;
import com.github.xiaoymin.deploy.common.PluginConst;
import com.github.xiaoymin.deploy.model.DeployRequest;
import com.github.xiaoymin.deploy.plugin.DeployService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 执行备份的操作,主要逻辑步骤
 *
 * <p>1.判断目标文件夹下是否存在backup文件夹,如果不存在则创建 </p>
 *
 * <p>2.判断目标文件是否存在,如果不存在则不执行备份策略</p>
 *
 * <p>3.如果目标文件已经存在,执行备份策略,拷贝目标文件至backup文件夹下</p>
 *
 * <p>4.完成备份</p>
 * @since:deploy-helper 1.1
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/05 13:26
 */
public class BackupService implements DeployService {

    Logger logger= LoggerFactory.getLogger(BackupService.class);

    @Override
    public boolean action(DeployRequest deployRequest) {
        logger.info("BackupService Run...");
        try {
            if (deployRequest.getHelperDeploy().isBackup()){
                logger.info("启动备份策略");
                DeployHelperConnection connection=deployRequest.getConnection();
                if (connection.exists(deployRequest.getHelperDeploy().getTarget())){
                    String targetBackupPath= StrUtil.subBefore(deployRequest.getHelperDeploy().getTarget(),"/",true)+"/backup";
                    //判断目标文件夹下是否存在backup文件夹,如果不存在则创建
                    connection.mkdir(targetBackupPath,true);
                    String targetBackupFileName=StrUtil.subAfter(deployRequest.getHelperDeploy().getTarget(),"/",true);
                    String timestamp=LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
                    String renameFile="";
                    //判断是否包含.
                    if (StrUtil.contains(targetBackupFileName,".")){
                        String backupName=StrUtil.subBefore(targetBackupFileName,".",true);
                        String type=StrUtil.subAfter(targetBackupFileName,".",true);
                        renameFile=backupName+"_"+timestamp+"."+type;
                    }else{
                        renameFile=targetBackupFileName+"_"+ timestamp;
                    }
                    String renameFilePath=targetBackupPath+"/"+renameFile;
                    logger.info("targetBackupfileName:{},renameFile:{}",targetBackupFileName,renameFile);
                    String shellCommand="cp "+deployRequest.getHelperDeploy().getTarget()+" "+renameFilePath;
                    connection.shell(shellCommand,true);
                }
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getPluginName() {
        return PluginConst.PLUGIN_BACKUP_SERVICE;
    }
}
