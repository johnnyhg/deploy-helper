/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.model;

import cn.hutool.core.util.StrUtil;

/***
 * 目标服务器的配置信息
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/02 11:00
 */
public class DeployHelperServer {

    private String profile;
    private String host;
    private Integer port;
    private String username;
    private String password;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DeployHelperServer{" +
                "profile='" + profile + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", username='" + username + '\'' +
                '}';
    }

    /**
     * 校验参数
     */
    public void validate(){
        if (StrUtil.isBlank(this.host)){
            throw new IllegalArgumentException("Host can't be empty!!");
        }
        if (StrUtil.isBlank(this.username)){
            throw new IllegalArgumentException("Username can't be empty!!!");
        }
        if (StrUtil.isBlank(this.password)){
            throw new IllegalArgumentException("Password can't be empty!!!");
        }
        if (this.port==null){
            throw new IllegalArgumentException("Port can't be null!!!");
        }
    }
}
