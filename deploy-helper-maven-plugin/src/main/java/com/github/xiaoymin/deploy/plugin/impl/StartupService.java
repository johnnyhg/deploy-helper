/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.deploy.plugin.impl;

import cn.hutool.core.util.StrUtil;
import com.github.xiaoymin.deploy.DeployHelperConnection;
import com.github.xiaoymin.deploy.common.PluginConst;
import com.github.xiaoymin.deploy.model.DeployHelperDeploy;
import com.github.xiaoymin.deploy.model.DeployRequest;
import com.github.xiaoymin.deploy.plugin.DeployService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/***
 * 启动任务
 * @since:deploy-helper 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/07/05 13:26
 */
public class StartupService implements DeployService {

    Logger logger= LoggerFactory.getLogger(StartupService.class);

    @Override
    public boolean action(DeployRequest deployRequest) {
        logger.info("StartupService Run...");
        DeployHelperConnection connection=deployRequest.getConnection();
        DeployHelperDeploy helperDeploy=deployRequest.getHelperDeploy();
        try{
            List<String> shells=new ArrayList<>();
            //执行start脚本
            if (StrUtil.isNotBlank(helperDeploy.getStartShell())){
                if (!connection.exists(helperDeploy.getStartShell())){
                    throw new RuntimeException("startShell don't exists");
                }
                //String startCommand="sh "+helperDeploy.getStartShell();
                String shDir=StrUtil.subBefore(helperDeploy.getStartShell(),"/",true);
                String shShell=StrUtil.subAfter(helperDeploy.getStartShell(),"/",true);
                shells.add("cd "+shDir);
                shells.add("./"+shShell);
                connection.shell(shells,true);
                //connection.shell(startCommand,true);
            }else{
                if (helperDeploy.isActiveDefaultStart()){
                    String targetPath=StrUtil.subBefore(helperDeploy.getTarget(),"/",true);
                    String targetShell=targetPath+"/"+"deployStartup.sh";
                    //判断目标文件是否存在
                    if (!connection.exists(targetShell)){
                        shells.add("echo 'nohup java -Xms512m -Xmx512m -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -XX:MaxNewSize=256m -jar "+helperDeploy.getTarget()+"  >> /dev/null 2>&1 &' > "+targetShell);
                        shells.add("chmod a+x "+targetShell);
                    }
                    shells.add("cd "+targetPath);
                    shells.add("./deployStartup.sh");
                    //shells.add("sh "+targetShell);
                    connection.shell(shells,true);
                }
            }
            return true;
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public String getPluginName() {
        return PluginConst.PLUGIN_START_SERVICE;
    }
}
